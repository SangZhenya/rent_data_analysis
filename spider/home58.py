# -*- coding: utf-8 -*-

from urllib import request
from bs4 import BeautifulSoup
import lxml
from pymongo import MongoClient
import json
import http.cookiejar
import time
import random
import socket

from getproxy import MyProxyPool

head = {
    'Connection': 'Keep-Alive',
    'Accept': 'text/html, application/xhtml+xml, */*',
    'Accept-Language': 'en-US,en;q=0.8,zh-Hans-CN;q=0.5,zh-Hans;q=0.3',
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko'
}

my_proxy = {}
error_count = 0
page_error = 0


class Home58(object):
    def __init__(self):
        db_helper = MongoClient('mongodb://localhost:27017/')
        db = db_helper['test']
        self.collection = db['markfive']
        self.proxy_pool = MyProxyPool()

    def get_info(self, page_number):
        url = "http://sh.58.com/chuzu/pn" + str(page_number) + "/"
        print(url)
        timeout = 10
        socket.setdefaulttimeout(timeout)
        cj = http.cookiejar.CookieJar()
        global my_proxy
        proxy_support = request.ProxyHandler(my_proxy)
        opener = request.build_opener(request.HTTPCookieProcessor(cj), proxy_support)
        header = []
        for key, value in head.items():
            elem = (key, value)
            header.append(elem)
        opener.addheaders = header
        info = opener.open(url).read()
        # info = info.decode('utf-8')

        soup = BeautifulSoup(info, "lxml")
        row_info = soup.find_all('div', class_='des')
        for x in range(len(row_info)):
            try:
                row = row_info[x]
                home_title = row.h2.get_text().strip()
                home_url = row.a.get('href')
                print(home_url)

                jjr_info = row.find_all('div', class_='jjr')
                if len(jjr_info) > 0:
                    print("From jjr")

                from_info = row.find_all('p', class_='gongyu')
                if len(from_info) > 0:
                    print(
                        "From *****************************************************************************************")

                geren_info = row.find_all('p', class_='geren')
                if len(geren_info) > 0:
                    print("From geren")

                home_detail = opener.open(home_url).read()
                home_detail = home_detail.decode('utf-8')
                detail_soup = BeautifulSoup(home_detail, 'lxml')
                detail_info = detail_soup.find_all('div', class_='house-desc-item')

                span_info = detail_info[0].find_all('span')
                home_price = span_info[0].get_text()
                home_pay_method = span_info[1].get_text()

                home_method = span_info[3].get_text()

                home_unit_size = span_info[5].get_text()
                home_unit = home_unit_size[:15].strip()

                size_index = home_unit_size[15:].strip().find('平')
                home_size = home_unit_size[15:].strip()[:size_index - 1].strip()
                home_deca = home_unit_size[15:].strip()[size_index + 1:].strip()

                towards_tall = span_info[7].get_text().strip()
                home_towards = towards_tall[:3].strip()
                home_tall = towards_tall[3:].strip()

                home_cell = span_info[9].get_text()

                home_area_info = span_info[11].get_text()
                home_area_0 = home_area_info[:6].strip()
                home_area_1 = home_area_info[6:].strip()

                home_way = span_info[13].get_text().strip()
                home_note = span_info[16].get_text().strip()

                map_info = detail_soup.find_all('div', class_='view-more-detailmap')
                home_lat = ''
                home_lon = ''
                if len(map_info) > 0:
                    map_url = map_info[0].a.get('href')
                    equal_index = map_url.find('=')
                    and_index = map_url.find('&')
                    position_info = map_url[equal_index + 1:and_index]
                    comet_index = position_info.find(',')
                    home_lat = position_info[:comet_index]
                    home_lon = position_info[comet_index + 1:]

                print(home_title)
                print(home_price + " " + home_pay_method)
                print(home_method)
                print(home_unit + " " + home_size + " " + home_deca)
                print(home_towards + " " + home_tall)
                print(home_cell)
                print(home_area_0 + " " + home_area_1)
                print(home_way)
                print(home_note)
                print(home_lat + ":" + home_lon)

                single_home = {'title': home_title,  # 标题
                               'price': home_price,  # 价格
                               'pay_method': home_pay_method,  # 支付方式
                               'method': home_method,  # 出租方式
                               'towards': home_towards,  # 朝向
                               'unit': home_unit,  # 户型
                               'size': home_size,  # 户型
                               'cell': home_cell,  # 小区
                               'decoration': home_deca,  # 装修方式
                               'floor': home_tall,  # 户高
                               'area_0': home_area_0,  # 区
                               'area_1': home_area_1,  # 县
                               'way': home_way,  # 所在路
                               'note': home_note,  # 注释
                               'longitude': home_lon,  # 经度
                               'latitude': home_lat  # 纬度
                               }
                self.collection.insert_one(single_home)
            except Exception as e:
                global error_count
                error_count += 1
                if error_count > 10:
                    my_proxy = self.proxy_pool.get_proxy()
                    error_count = 0
                print("error_count:" + str(error_count) + ",first error", end=':')
                print(e)
            print(str(x) + '--------------------------------------')
        print('\n' + str(page_number) + '========================================\n')

    def get_all(self):
        fail_page = set()
        for i in range(1, 100):
            # self.get_info(i)
            try:
                self.get_info(i)
                time.sleep(random.randint(0, 60))
            except Exception as e:
                fail_page.add(i)
                global page_error
                page_error += 1
                if page_error > 2:
                    global my_proxy
                    my_proxy = self.proxy_pool.get_proxy()
                    page_error = 0
                print("page_error:" + str(page_error) + ",second error", end=':')
                print(e)
        print(fail_page)


if __name__ == '__main__':
    first_proxy = MyProxyPool()
    my_proxy = first_proxy.get_proxy()
    home58 = Home58()
    home58.get_all()
