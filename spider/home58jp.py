# -*- coding: utf-8 -*-

from urllib import request
from bs4 import BeautifulSoup
import lxml
from pymongo import MongoClient
import json
import http.cookiejar
import time
import random
import re

head = {
    'Connection': 'Keep-Alive',
    'Accept': 'text/html, application/xhtml+xml, */*',
    'Accept-Language': 'en-US,en;q=0.8,zh-Hans-CN;q=0.5,zh-Hans;q=0.3',
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko'
}


def get_info(page_number, homedb):
    url = "http://sh.58.com/pinpaigongyu/pn/" + str(page_number) + "/"

    cj = http.cookiejar.CookieJar()
    opener = request.build_opener(request.HTTPCookieProcessor(cj))
    header = []
    for key, value in head.items():
        elem = (key, value)
        header.append(elem)
    opener.addheaders = header
    info = opener.open(url).read()
    # info = info.decode('utf-8')

    soup = BeautifulSoup(info, "lxml")
    ul_info = soup.find_all('ul', class_='list')
    row_info = ul_info[0].find_all('a')
    for x in range(len(row_info)):
        row = row_info[x]
        home_url = 'http://sh.58.com' + row.get('href')
        print(home_url)

        home_detail = opener.open(home_url).read()
        home_detail = home_detail.decode('utf-8')
        detail_soup = BeautifulSoup(home_detail, 'lxml')

        home_title = detail_soup.h2.get_text()
        home_price = detail_soup.find_all('span', class_='price')

        detail_info = detail_soup.find_all('ul', class_='house-info-list')
        li_info = detail_info[0].find_all('li')
        home_size = li_info[0].span.get_text()
        unit_method = li_info[1].span.get_text().strip()
        home_unit = unit_method[:15].strip()
        method_towards = unit_method[15:].strip()
        method_index = method_towards.find('[')
        home_method = ''
        home_toward = unit_method[15:].strip()
        if method_index is not -1:
            home_toward = method_towards[:method_index].strip()
            home_method = method_towards[method_index:].strip()

        home_floor = li_info[2].span.get_text().strip()
        home_way = li_info[3].span.get_text()
        home_subway = li_info[4].span.get_text()

        position_re = r"____json4fe.lon = '(\S*)';"
        home_lon = re.findall(position_re, home_detail)

        position_re = r" ____json4fe.lat = '(\S*)';"
        home_lat = re.findall(position_re, home_detail)

        print(home_title)
        print(home_price[0].get_text())
        print(home_size)
        print(home_unit + " " + home_toward + " " + home_method)
        print(home_floor)
        print(home_way)
        print(home_subway)
        print(home_lon[0] + " " + home_lat[0])

        single_home = {'title': home_title,  # 标题
                       'price': home_price[0].get_text(),  # 价格
                       'method': home_method,  # 出租方式
                       'unit': home_unit,  # 户型
                       'size': home_size,  # 面积
                       'floor': home_floor,  # 楼高
                       'way': home_way,  # 所在路
                       'towards': home_toward,  # 朝向
                       'subway': home_subway,  # 地铁
                       'longitude': home_lon[0],  # 经度
                       'latitude': home_lat[0]  # 维度
                       }
        homedb.insert_one(single_home)
        print(str(x) + '--------------------------------------')
        time.sleep(random.randint(3, 5))
    print('\n' + str(page_number) + '========================================\n')


def get_all(home):
    for i in range(1, 200):
        try:
            get_info(i, home)
            time.sleep(random.randint(0, 60))
        except Exception as e:
            print(e)


if __name__ == '__main__':
    db_helper = MongoClient('mongodb://localhost:27017/')
    db = db_helper['test']
    collection = db['markfivejp']
    get_all(collection)
