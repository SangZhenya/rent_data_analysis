#! /usr/bin/env python
# -*- coding: utf-8 -*-


import re
from urllib import request
import random
from bs4 import BeautifulSoup
import http.cookiejar
import socket

head = {
    'Connection': 'Keep-Alive',
    'Accept': 'text/html, application/xhtml+xml, */*',
    'Accept-Language': 'en-US,en;q=0.8,zh-Hans-CN;q=0.5,zh-Hans;q=0.3',
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko'
}


class MyGatherProxy(object):
    url = 'http://www.xicidaili.com/wt/'
    pre1 = re.compile(r'<td>(\d\S*)</td>')

    def get_elite(self, pages=1):
        proxies = set()
        for i in range(pages):
            cj = http.cookiejar.CookieJar()
            opener = request.build_opener(request.HTTPCookieProcessor(cj))
            header = []
            for key, value in head.items():
                elem = (key, value)
                header.append(elem)
            opener.addheaders = header
            page_url = self.url + str(i + 1)
            print(page_url)
            info = opener.open(page_url).read()
            info = info.decode('utf-8')
            proxy_info = self.pre1.findall(info)
            for proxy_index in range(0, len(proxy_info), 3):
                proxies.add(proxy_info[proxy_index] + ":" + proxy_info[proxy_index + 1])
        return proxies


class MyProxyPool(object):
    gather_proxy = MyGatherProxy()

    def __init__(self):
        self.pool = set()

    def update_gather_proxy(self, pages=1):
        self.pool.update(self.gather_proxy.get_elite(pages=pages))

    def remove_proxy(self, proxy):
        if proxy in self.pool:
            self.pool.remove(proxy)

    def random_choose(self):
        if self.pool:
            return random.sample(self.pool, 1)[0]
        else:
            self.update_gather_proxy(3)
            return random.sample(self.pool, 1)[0]

    def get_proxy(self):
        proxy = self.random_choose()
        proxies = {'http': 'http://' + proxy}
        try:
            print(proxies)
            timeout = 2
            socket.setdefaulttimeout(timeout)
            proxy_support = request.ProxyHandler(proxies)
            opener = request.build_opener(proxy_support)
            request.install_opener(opener)
            response = request.urlopen("http://www.sangzhenya.com/")
            if response.getcode() == 200:
                print(response.read())
                return proxies
            else:
                self.remove_proxy(proxy)
                return self.get_proxy()
        except Exception as e:
            print("代理服务器异常:", end='')
            print(e)
            self.remove_proxy(proxy)
            return self.get_proxy()


if __name__ == '__main__':
    proxy_pool = MyProxyPool()
    test_info = proxy_pool.get_proxy()
    print(test_info)
