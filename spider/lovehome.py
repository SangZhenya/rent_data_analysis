# -*- coding: utf-8 -*-

import http.cookiejar
import random
import time
from urllib import request

from bs4 import BeautifulSoup
from pymongo import MongoClient

head = {
    'Connection': 'Keep-Alive',
    'Accept': 'text/html, application/xhtml+xml, */*',
    'Accept-Language': 'en-US,en;q=0.8,zh-Hans-CN;q=0.5,zh-Hans;q=0.3',
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko'
}


def get_info(page_number, homedb):
    url = "http://sh.lianjia.com/zufang/d" + str(page_number)

    cj = http.cookiejar.CookieJar()
    opener = request.build_opener(request.HTTPCookieProcessor(cj))
    header = []
    for key, value in head.items():
        elem = (key, value)
        header.append(elem)
    opener.addheaders = header
    info = opener.open(url).read()
    info = info.decode('utf-8')

    soup = BeautifulSoup(info, "lxml")
    for row in soup.find_all('div', class_='info-panel'):
        home_url = row.h2.a.get('href')
        print(home_url)
        home_title = row.h2.a.get_text()  # 标题
        print(home_title)
        cols = row.div.find_all('div')
        home_info = cols[0].find_all('span')

        home_cell = home_info[0].get_text()  # 小区
        home_util = home_info[1].get_text().strip()  # 户型
        home_size = home_info[2].get_text().strip()  # 户型大小

        print(home_cell + " " + home_util + " " + home_size)

        home_place = cols[1].find_all('a')
        floor_info = cols[1].get_text()
        floor_info = floor_info[20:].strip()
        floor_info_number = floor_info[:10].strip()
        floor_info = floor_info[10:].strip()
        towards_info = floor_info[2:]

        home_area_0 = home_place[0].get_text()  # 区
        home_area_1 = home_place[1].get_text()  # 县
        home_floor = floor_info_number  # 楼层
        home_towards = towards_info  # 朝向

        print(home_area_0 + " " + home_area_1 + " " + home_floor + " " + home_towards)

        home_subway = cols[3].find_all('span', class_='fang-subway-ex')  # 地铁
        home_visit = cols[3].find_all('span', class_='anytime-ex')  # 看房

        home_subway_info = home_subway[0].get_text()
        home_visit_info = home_visit[0].get_text()

        print(home_subway_info + " " + home_visit_info)

        home_url = "http://sh.lianjia.com" + home_url
        home_detail = opener.open(home_url).read()
        home_detail = home_detail.decode('utf-8')

        detail_soup = BeautifulSoup(home_detail, 'lxml')
        detail_info = detail_soup.find_all('div', class_='mainInfo')

        home_price = detail_info[0].get_text()  # 价格

        print(home_price)

        way_info = detail_soup.find_all('p', class_='addrEllipsis')

        home_area_2 = way_info[1].get_text().strip()  # 路

        print(home_area_2)
        img_object = detail_soup.find(id="img_huxing")
        img_url = ''  # 户型图
        if img_object is not None:
            img_url = img_object.get('src')
        print(img_url)

        home_longitude = ''
        home_latitude = ''
        home_position = detail_soup.find(id='zoneMap')
        if home_position is not None:
            home_longitude = home_position.get('longitude')
            home_latitude = home_position.get('latitude')

        print(home_longitude + ':' + home_latitude)

        single_home = {'title': home_title,  # 标题
                       'cell': home_cell,  # 小区
                       'unit': home_util,  # 户型
                       'size': home_size,  # 大小
                       'towards': home_towards,  # 朝向
                       'price': home_price,  # 价格
                       'floor': home_floor,  # 楼层
                       'area_0': home_area_0,  # 区
                       'area_1': home_area_1,  # 县
                       'area_2': home_area_2,  # 路
                       'longitude': home_longitude,  # 经度
                       'latitude': home_latitude,  # 维度
                       'subway': home_subway_info,  # 地铁信息
                       'visit': home_visit_info,  # 看房信息
                       'img_url': img_url  # 户型图
                       }
        homedb.insert_one(single_home)
        print('--------------------------------------')
        time.sleep(random.randint(5, 10))
    print('\n' + str(page_number) + '========================================\n')


def get_all(home):
    for i in range(1, 200):
        try:
            get_info(i, home)
            time.sleep(random.randint(30, 60))
        except Exception as e:
            print(e)


if __name__ == '__main__':
    db_helper = MongoClient('mongodb://localhost:27017/')
    db = db_helper['test']
    collection = db['home']
    get_all(collection)
    # get_info(0)
