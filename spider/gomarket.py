# -*- coding: utf-8 -*-

import http.cookiejar
import json
import random
import time
from urllib import request

import lxml
from bs4 import BeautifulSoup
from pymongo import MongoClient

head = {
    'Connection': 'Keep-Alive',
    'Accept': 'text/html, application/xhtml+xml, */*',
    'Accept-Language': 'en-US,en;q=0.8,zh-Hans-CN;q=0.5,zh-Hans;q=0.3',
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko'
}


def get_info(page_number, homedb):
    url = "http://sh.ganji.com/fang1/o" + str(page_number)

    cj = http.cookiejar.CookieJar()
    opener = request.build_opener(request.HTTPCookieProcessor(cj))
    header = []
    for key, value in head.items():
        elem = (key, value)
        header.append(elem)
    opener.addheaders = header
    info = opener.open(url).read()
    info = info.decode('utf-8')

    i = 0
    soup = BeautifulSoup(info, "lxml")
    row_info = soup.find_all('div', class_='f-list-item')
    for x in range(len(row_info)):
        row = row_info[x]
        home_info = row.find_all('dd', class_='dd-item')
        if len(home_info) > 2:
            home_title = home_info[0].a.get_text()
            home_url = home_info[0].a.get('href')
            detail_info = home_info[1].find_all('span')

            price_info = row.find_all('div', class_='price')
            if len(price_info) > 0:
                price = price_info[0].get_text()
                home_method = detail_info[0].get_text()
                home_unit = detail_info[2].get_text()
                if home_unit.endswith('㎡'):
                    home_size = home_unit
                    home_unit = "None"
                    home_towards = detail_info[4].get_text()
                    home_tall = "None"
                    home_decoration = "None"
                    if len(detail_info) > 5:
                        home_tall = detail_info[6].get_text().strip()
                    if len(detail_info) > 7:
                        home_decoration = detail_info[8].get_text().strip()
                else:
                    home_size = detail_info[4].get_text()
                    home_towards = detail_info[6].get_text()
                    home_tall = "None"
                    home_decoration = "None"
                    if len(detail_info) > 7:
                        home_tall = detail_info[8].get_text().strip()
                    if len(detail_info) > 9:
                        home_decoration = detail_info[10].get_text().strip()
                print(home_title)
                print(price + " " + home_method + " " + home_towards + " " +
                      home_unit + " " + home_size + " " +
                      home_tall + " " + home_decoration)
                home_area_info = home_info[2].find_all('a')
                if len(home_area_info) >= 2:
                    home_area_0 = home_area_info[0].get_text().strip()
                    home_area_1 = home_area_info[1].get_text().strip()
                    home_area_2 = home_info[2].get_text()[100:].strip()
                    print(home_area_0 + ' ' + home_area_1 + ' ' + home_area_2)

                    home_url = "http://sh.ganji.com" + home_url
                    home_detail = opener.open(home_url).read()
                    home_detail = home_detail.decode('utf-8')
                    detail_soup = BeautifulSoup(home_detail, 'lxml')
                    meta_info = detail_soup.find_all('meta')
                    home_position = meta_info[3].get('content')

                    position_info = home_position.split(';')
                    detail_position_info = position_info[2][6:].split(',')
                    home_longitude = detail_position_info[0]
                    home_latitude = detail_position_info[1]
                    print(home_longitude + ':' + home_latitude)

                    single_home = {'title': home_title,  # 标题
                                   'price': price,  # 价格
                                   'method': home_method,  # 出租方式
                                   'towards': home_towards,  # 朝向
                                   'unit': home_unit,  # 户型
                                   'size': home_size,  # 大小
                                   'decoration': home_decoration,  # 装修方式
                                   'floor': home_tall,  # 楼高
                                   'area_0': home_area_0,  # 区
                                   'area_1': home_area_1,  # 县
                                   'area_2': home_area_2,  # 小区
                                   'longitude': home_longitude,  # 经度
                                   'latitude': home_latitude  # 维度
                                   }
                    homedb.insert_one(single_home)
        print(str(x) + '--------------------------------------')
        # time.sleep(random.randint(5, 10))
    print('\n' + str(page_number) + '========================================\n')


def get_all(home):
    for i in range(37, 200):
        try:
            get_info(i, home)
            time.sleep(random.randint(0, 60))
        except Exception as e:
            print(e)


if __name__ == '__main__':
    db_helper = MongoClient('mongodb://localhost:27017/')
    db = db_helper['test']
    collection = db['gtmarket']
    get_all(collection)
    # get_info(3)
