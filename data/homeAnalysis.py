import xlsxwriter
from pymongo import MongoClient

db_helper = MongoClient('mongodb://localhost:27017/')
db = db_helper['test']

workbook = xlsxwriter.Workbook('data004.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write(0, 0, "标题")
worksheet.write(0, 1, "价格（元/月）")
worksheet.write(0, 2, "户型")
worksheet.write(0, 3, "大小（平）")

worksheet.write(0, 4, "楼层")
worksheet.write(0, 5, "楼高(层)")
worksheet.write(0, 6, "朝向")
worksheet.write(0, 7, "区")

worksheet.write(0, 8, "县")
worksheet.write(0, 9, "路")
worksheet.write(0, 10, "小区")
worksheet.write(0, 11, "纬度")

worksheet.write(0, 12, "经度")
worksheet.write(0, 13, "地铁信息")
worksheet.write(0, 14, "户型图")
worksheet.write(0, 15, "看房")

data_index = 1
for raw_data in db.home.find():
    house_title = raw_data['title']
    price_info = raw_data['price']
    price_index = price_info.find('元')

    house_price = price_info[:price_index]

    price_float = float(house_price)

    house_unit = raw_data['unit']
    house_size = raw_data['size'][:-1]

    size_float = float(house_size)

    floor_info = raw_data['floor']
    floor_index = floor_info.find('/')

    house_floor = floor_info[:floor_index]
    house_tall = floor_info[floor_index + 1:-1]

    tall_float = float(house_tall)

    house_towards = raw_data['towards']
    house_area_0 = raw_data['area_0']

    house_area_1 = raw_data['area_1']
    house_area_2 = raw_data['area_2']
    house_cell = raw_data['cell']
    house_latitude = raw_data['latitude']  # 纬度
    house_longitude = raw_data['longitude']  # 经度

    longitude_float = float(house_longitude) - 0.012774687519  # 经度修正
    latitude_float = float(house_latitude) - 0.00394531687912  # 纬度修正
    house_subway = raw_data['subway']
    house_img_url = raw_data['img_url']
    house_visit = raw_data['visit']

    worksheet.write(data_index, 0, house_title)
    worksheet.write(data_index, 1, price_float)
    worksheet.write(data_index, 2, house_unit)
    worksheet.write(data_index, 3, size_float)

    worksheet.write(data_index, 4, house_floor)
    worksheet.write(data_index, 5, tall_float)
    worksheet.write(data_index, 6, house_towards)
    worksheet.write(data_index, 7, house_area_0)

    worksheet.write(data_index, 8, house_area_1)
    worksheet.write(data_index, 9, house_area_2)
    worksheet.write(data_index, 10, house_cell)
    worksheet.write(data_index, 11, latitude_float)

    worksheet.write(data_index, 12, longitude_float)
    worksheet.write(data_index, 13, house_subway)
    worksheet.write(data_index, 14, house_img_url)
    worksheet.write(data_index, 15, house_visit)

    print(str(data_index))
    data_index += 1

workbook.close()
