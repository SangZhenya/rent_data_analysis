import xlsxwriter
from pymongo import MongoClient
import random

db_helper = MongoClient('mongodb://localhost:27017/')
db = db_helper['test']

workbook = xlsxwriter.Workbook('tatalData.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write(0, 0, "标题")
worksheet.write(0, 1, "价格（元/月）")
worksheet.write(0, 2, "支付方式")
worksheet.write(0, 3, "户型")

worksheet.write(0, 4, "面积（平）")
worksheet.write(0, 5, "楼层")
worksheet.write(0, 6, "楼高(层)")
worksheet.write(0, 7, "朝向")

worksheet.write(0, 8, "出租方式")
worksheet.write(0, 9, "装修情况")
worksheet.write(0, 10, "备注")
worksheet.write(0, 11, "区")

worksheet.write(0, 12, "县")
worksheet.write(0, 13, "路")
worksheet.write(0, 14, "小区")
worksheet.write(0, 15, "纬度")

worksheet.write(0, 16, "经度")
worksheet.write(0, 17, "地铁信息")
worksheet.write(0, 18, "户型图")
worksheet.write(0, 19, "看房情况")

data_index = 1

# 用于处理从链家获取的数据
for raw_data in db.home.find():
    house_title = raw_data['title']
    price_info = raw_data['price']
    price_index = price_info.find('元')

    house_price = price_info[:price_index]

    price_float = float(house_price)

    house_unit = raw_data['unit']
    house_size = raw_data['size'][:-1]

    size_float = float(house_size)

    floor_info = raw_data['floor']
    floor_index = floor_info.find('/')

    house_floor = floor_info[:floor_index]
    house_tall = floor_info[floor_index + 1:-1]

    tall_float = float(house_tall)

    house_towards = raw_data['towards']
    house_area_0 = raw_data['area_0']

    house_area_1 = raw_data['area_1']
    house_area_2 = raw_data['area_2']
    house_cell = raw_data['cell']
    house_latitude = raw_data['latitude']  # 纬度
    house_longitude = raw_data['longitude']  # 经度

    longitude_float = float(house_longitude) - 0.012774687519  # 经度修正
    latitude_float = float(house_latitude) - 0.00394531687912  # 纬度修正
    house_subway = raw_data['subway']
    house_img_url = raw_data['img_url']
    house_visit = raw_data['visit']

    worksheet.write(data_index, 0, house_title)
    worksheet.write(data_index, 1, price_float)
    worksheet.write(data_index, 3, house_unit)
    worksheet.write(data_index, 4, size_float)

    worksheet.write(data_index, 5, house_floor)
    worksheet.write(data_index, 6, tall_float)
    worksheet.write(data_index, 7, house_towards)
    worksheet.write(data_index, 11, house_area_0)

    worksheet.write(data_index, 12, house_area_1)
    worksheet.write(data_index, 13, house_area_2)
    worksheet.write(data_index, 14, house_cell)
    worksheet.write(data_index, 15, longitude_float)

    worksheet.write(data_index, 16, latitude_float)
    worksheet.write(data_index, 17, house_subway)
    worksheet.write(data_index, 18, house_img_url)
    worksheet.write(data_index, 19, house_visit)

    print(str(data_index))
    data_index += 1

# 用于处理从赶集网获取的数据
for raw_data in db.gtmarket.find():
    house_title = raw_data['title']  # 1标题

    price_info = raw_data['price']
    price_index = price_info.find('元')
    house_price = price_info[:price_index]
    price_float = float(house_price)  # 2价格

    house_unit = raw_data['unit']  # 3户型
    house_size = raw_data['size'][:-1]

    size_float = float(house_size)  # 4面积

    floor_info = raw_data['floor']

    floor_index = floor_info.find('）')
    house_floor = ''  # 5楼层
    house_tall = ''  # 6楼高
    house_decoration = ''  # 7装修方式
    if floor_info != 'None' and floor_info.find("装") == -1:
        if floor_index == -1:
            house_tall = floor_info[1:-1]
        else:
            floor_index_1 = floor_info.find('共')
            house_floor = floor_info[:floor_index_1 - 1]
            floor_index_2 = floor_info.rfind('层')
            real_index = floor_index if floor_index < floor_index_2 else floor_index_2
            house_tall = floor_info[floor_index_1 + 1:real_index]
    elif floor_info.find("装") != -1:
        house_decoration = floor_info
        house_floor = ""
        house_tall = "5"
    elif raw_data['towards'].find("共") != -1:
        floor_info = raw_data['towards']
        floor_index = floor_info.find('）')
        if floor_index == -1:
            house_tall = floor_info[1:-1]
        else:
            floor_index_1 = floor_info.find('共')
            house_floor = floor_info[:floor_index_1 - 1]
            floor_index_2 = floor_info.rfind('层')
            real_index = floor_index if floor_index < floor_index_2 else floor_index_2
            house_tall = floor_info[floor_index_1 + 1:real_index]

    if house_decoration == '' and raw_data['decoration'].find('共') == -1:
        house_decoration = raw_data['decoration']

    tall_float = float(house_tall)

    house_method = raw_data['method']  # 8出租方式
    house_area_0 = raw_data['area_0']  # 9区
    house_area_1 = raw_data['area_1']  # 10县
    house_cell = raw_data['area_2']  # 11小区

    house_towards = "南北向"
    if raw_data['towards'].find('共') == -1:
        house_towards = raw_data['towards']  # 12朝向

    house_latitude = raw_data['latitude']  # 13纬度
    house_longitude = raw_data['longitude']  # 14经度

    longitude_float = float(house_longitude) - 0.012774687519  # 经度修正
    latitude_float = float(house_latitude) - 0.00394531687912  # 纬度修正

    worksheet.write(data_index, 0, house_title)
    worksheet.write(data_index, 1, price_float)
    worksheet.write(data_index, 3, house_unit)
    worksheet.write(data_index, 4, size_float)

    worksheet.write(data_index, 5, house_floor)
    worksheet.write(data_index, 6, tall_float)
    worksheet.write(data_index, 7, house_towards)
    worksheet.write(data_index, 8, house_method)

    worksheet.write(data_index, 9, house_decoration)
    worksheet.write(data_index, 11, house_area_0)
    worksheet.write(data_index, 12, house_area_1)
    worksheet.write(data_index, 14, house_cell)

    worksheet.write(data_index, 15, longitude_float)
    worksheet.write(data_index, 16, latitude_float)

    print(str(data_index))
    data_index += 1

# 用于处理从58同城获取的信息
for raw_data in db.markfive.find():
    house_title = raw_data['title']  # 1标题
    price_info = raw_data['price']  # 2价格
    price_index = price_info.find('元')
    house_price = price_info[:price_index]
    price_float = float(house_price)

    house_unit = raw_data['unit']  # 3户型
    house_size = raw_data['size']  # 4大小
    if house_size != '':
        size_float = float(house_size)
    else:
        size_float = random.randint(60, 100)

    house_pay = raw_data['pay_method']  # 5支付方式
    house_method = raw_data['method']  # 6出租方式

    floor_info = raw_data['floor']
    floor_index = floor_info.find('/')
    if floor_index == -1:
        house_floor = '3层'
        house_tall = '5'
    else:
        house_floor = floor_info[:floor_index]  # 7楼层
        house_tall = floor_info[floor_index + 2:-1]  # 8楼高

    tall_float = float(house_tall)

    house_towards = raw_data['towards']  # 9朝向
    house_decoration = raw_data['decoration']  # 10装修方式
    house_note = raw_data['note']  # 11备注
    house_area_0 = raw_data['area_0']  # 12区

    house_area_1 = raw_data['area_1']  # 13县
    house_area_2 = raw_data['way']  # 14路
    house_cell = raw_data['cell']  # 15小区
    house_latitude = raw_data['latitude']  # 16纬度
    house_longitude = raw_data['longitude']  # 17经度

    if house_latitude == '':
        house_latitude = '0.012774687519'
        house_longitude = '0.00394531687912'

    longitude_float = float(house_longitude) - 0.012774687519  # 经度修正
    latitude_float = float(house_latitude) - 0.00394531687912  # 纬度修正

    worksheet.write(data_index, 0, house_title)
    worksheet.write(data_index, 1, price_float)
    worksheet.write(data_index, 3, house_unit)
    worksheet.write(data_index, 4, size_float)

    worksheet.write(data_index, 2, house_pay)
    worksheet.write(data_index, 8, house_method)
    worksheet.write(data_index, 5, house_floor)
    worksheet.write(data_index, 6, tall_float)

    worksheet.write(data_index, 7, house_towards)
    worksheet.write(data_index, 9, house_decoration)
    worksheet.write(data_index, 10, house_note)
    worksheet.write(data_index, 11, house_area_0)

    worksheet.write(data_index, 12, house_area_1)
    worksheet.write(data_index, 13, house_area_2)
    worksheet.write(data_index, 14, house_cell)
    worksheet.write(data_index, 15, longitude_float)
    worksheet.write(data_index, 16, latitude_float)

    print(str(data_index))
    data_index += 1

# 用于处理从58精品公寓获取的租房信息
for raw_data in db.markfivejp.find():
    house_title = raw_data['title']  # 1标题
    price_info = raw_data['price']  # 2价格
    price_float = float(price_info)

    house_unit = raw_data['unit']  # 3户型
    house_size = raw_data['size'][:-2]  # 4大小
    if house_size != '':
        size_float = float(house_size)
    else:
        size_float = random.randint(60, 100)

    floor_info = raw_data['floor']
    floor_index = floor_info.find('/')
    if floor_index == -1:
        house_floor = '3层'
        house_tall = '5'
    else:
        house_floor = floor_info[:floor_index]  # 5楼层
        house_tall = floor_info[floor_index + 1:-1]  # 6楼高

    tall_float = float(house_tall)

    house_towards = raw_data['towards']  # 7朝向
    house_method = raw_data['method']  # 8出租方式
    house_subway = raw_data['subway']  # 9地铁信息
    house_way = raw_data['way']  # 10路
    house_latitude = raw_data['latitude']  # 11纬度
    house_longitude = raw_data['longitude']  # 12经度

    if house_latitude == '':
        house_latitude = '0.012774687519'
        house_longitude = '0.00394531687912'

    longitude_float = float(house_longitude) - 0.012774687519  # 经度修正
    latitude_float = float(house_latitude) - 0.00394531687912  # 纬度修正

    worksheet.write(data_index, 0, house_title)
    worksheet.write(data_index, 1, price_float)
    worksheet.write(data_index, 3, house_unit)
    worksheet.write(data_index, 4, size_float)

    worksheet.write(data_index, 5, house_floor)
    worksheet.write(data_index, 6, tall_float)
    worksheet.write(data_index, 7, house_towards)
    worksheet.write(data_index, 8, house_method)

    worksheet.write(data_index, 17, house_subway)
    worksheet.write(data_index, 13, house_way)
    worksheet.write(data_index, 15, longitude_float)
    worksheet.write(data_index, 16, latitude_float)

    print(str(data_index))
    data_index += 1

workbook.close()
