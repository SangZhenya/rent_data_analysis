import xlsxwriter
from pymongo import MongoClient
import random

db_helper = MongoClient('mongodb://localhost:27017/')
db = db_helper['test']

workbook = xlsxwriter.Workbook('fivejp.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write(0, 0, "标题")
worksheet.write(0, 1, "价格（元/月）")
worksheet.write(0, 2, "户型")
worksheet.write(0, 3, "大小（平）")

worksheet.write(0, 4, "楼层")
worksheet.write(0, 5, "楼高(层)")
worksheet.write(0, 6, "朝向")
worksheet.write(0, 7, "出租方式")

worksheet.write(0, 8, "地铁信息")
worksheet.write(0, 9, "路")
worksheet.write(0, 10, "纬度")
worksheet.write(0, 11, "经度")

data_index = 1
for raw_data in db.markfivejp.find():
    house_title = raw_data['title']  # 1标题
    price_info = raw_data['price']  # 2价格
    price_float = float(price_info)

    house_unit = raw_data['unit']  # 3户型
    house_size = raw_data['size'][:-2]  # 4大小
    if house_size != '':
        size_float = float(house_size)
    else:
        size_float = random.randint(60, 100)

    floor_info = raw_data['floor']
    floor_index = floor_info.find('/')
    if floor_index == -1:
        house_floor = '3层'
        house_tall = '5'
    else:
        house_floor = floor_info[:floor_index]  # 5楼层
        house_tall = floor_info[floor_index + 1:-1]  # 6楼高

    tall_float = float(house_tall)

    house_towards = raw_data['towards']  # 7朝向
    house_method = raw_data['method']  # 8出租方式
    house_subway = raw_data['subway']  # 9地铁信息
    house_way = raw_data['way']  # 10路
    house_latitude = raw_data['latitude']  # 11纬度
    house_longitude = raw_data['longitude']  # 12经度

    if house_latitude == '':
        house_latitude = '0.012774687519'
        house_longitude = '0.00394531687912'

    longitude_float = float(house_longitude) - 0.012774687519  # 经度修正
    latitude_float = float(house_latitude) - 0.00394531687912  # 纬度修正

    worksheet.write(data_index, 0, house_title)
    worksheet.write(data_index, 1, price_float)
    worksheet.write(data_index, 2, house_unit)
    worksheet.write(data_index, 3, size_float)

    worksheet.write(data_index, 4, house_floor)
    worksheet.write(data_index, 5, tall_float)
    worksheet.write(data_index, 6, house_towards)
    worksheet.write(data_index, 7, house_method)

    worksheet.write(data_index, 8, house_subway)
    worksheet.write(data_index, 9, house_way)
    worksheet.write(data_index, 10, longitude_float)
    worksheet.write(data_index, 11, latitude_float)

    print(str(data_index))
    data_index += 1

workbook.close()
