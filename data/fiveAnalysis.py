import xlsxwriter
from pymongo import MongoClient
import random

db_helper = MongoClient('mongodb://localhost:27017/')
db = db_helper['test']

workbook = xlsxwriter.Workbook('five.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write(0, 0, "标题")
worksheet.write(0, 1, "价格（元/月）")
worksheet.write(0, 2, "户型")
worksheet.write(0, 3, "大小（平）")

worksheet.write(0, 4, "支付方式")
worksheet.write(0, 5, "出租方式")
worksheet.write(0, 6, "楼层")
worksheet.write(0, 7, "楼高(层)")

worksheet.write(0, 8, "朝向")
worksheet.write(0, 9, "装修方式")
worksheet.write(0, 10, "备注")
worksheet.write(0, 11, "区")

worksheet.write(0, 12, "县")
worksheet.write(0, 13, "路")
worksheet.write(0, 14, "小区")
worksheet.write(0, 15, "纬度")

worksheet.write(0, 16, "经度")

data_index = 1
for raw_data in db.markfive.find():
    house_title = raw_data['title']  # 1标题
    price_info = raw_data['price']  # 2价格
    price_index = price_info.find('元')
    house_price = price_info[:price_index]
    price_float = float(house_price)

    house_unit = raw_data['unit']  # 3户型
    house_size = raw_data['size']  # 4大小
    if house_size != '':
        size_float = float(house_size)
    else:
        size_float = random.randint(60, 100)

    house_pay = raw_data['pay_method']  # 5支付方式
    house_method = raw_data['method']  # 6出租方式

    floor_info = raw_data['floor']
    floor_index = floor_info.find('/')
    if floor_index == -1:
        house_floor = '3层'
        house_tall = '5'
    else:
        house_floor = floor_info[:floor_index]  # 7楼层
        house_tall = floor_info[floor_index + 2:-1]  # 8楼高

    tall_float = float(house_tall)

    house_towards = raw_data['towards']  # 9朝向
    house_decoration = raw_data['decoration']  # 10装修方式
    house_note = raw_data['note']  # 11备注
    house_area_0 = raw_data['area_0']  # 12区

    house_area_1 = raw_data['area_1']  # 13县
    house_area_2 = raw_data['way']  # 14路
    house_cell = raw_data['cell']  # 15小区
    house_latitude = raw_data['latitude']  # 16纬度
    house_longitude = raw_data['longitude']  # 17经度

    if house_latitude == '':
        house_latitude = '0.012774687519'
        house_longitude = '0.00394531687912'

    longitude_float = float(house_longitude) - 0.012774687519  # 经度修正
    latitude_float = float(house_latitude) - 0.00394531687912  # 纬度修正

    worksheet.write(data_index, 0, house_title)
    worksheet.write(data_index, 1, price_float)
    worksheet.write(data_index, 2, house_unit)
    worksheet.write(data_index, 3, size_float)

    worksheet.write(data_index, 4, house_pay)
    worksheet.write(data_index, 5, house_method)
    worksheet.write(data_index, 6, house_floor)
    worksheet.write(data_index, 7, tall_float)

    worksheet.write(data_index, 8, house_towards)
    worksheet.write(data_index, 9, house_decoration)
    worksheet.write(data_index, 10, house_note)
    worksheet.write(data_index, 11, house_area_0)

    worksheet.write(data_index, 12, house_area_1)
    worksheet.write(data_index, 13, house_area_2)
    worksheet.write(data_index, 14, house_cell)
    worksheet.write(data_index, 15, longitude_float)
    worksheet.write(data_index, 16, latitude_float)

    print(str(data_index))
    data_index += 1

workbook.close()
