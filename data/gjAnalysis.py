import xlsxwriter
from pymongo import MongoClient

db_helper = MongoClient('mongodb://localhost:27017/')
db = db_helper['test']

workbook = xlsxwriter.Workbook('gtMarket.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write(0, 0, "标题")
worksheet.write(0, 1, "价格（元/月）")
worksheet.write(0, 2, "户型")
worksheet.write(0, 3, "面积（平）")

worksheet.write(0, 4, "楼层")
worksheet.write(0, 5, "楼高(层)")
worksheet.write(0, 6, "朝向")
worksheet.write(0, 7, "出租方式")

worksheet.write(0, 8, "装修")
worksheet.write(0, 9, "区")
worksheet.write(0, 10, "县")
worksheet.write(0, 11, "小区")

worksheet.write(0, 12, "纬度")
worksheet.write(0, 13, "经度")

data_index = 1
for raw_data in db.gtmarket.find():
    house_title = raw_data['title']  # 1标题

    price_info = raw_data['price']
    price_index = price_info.find('元')
    house_price = price_info[:price_index]
    price_float = float(house_price)  # 2价格

    house_unit = raw_data['unit']  # 3户型
    house_size = raw_data['size'][:-1]

    size_float = float(house_size)  # 4面积

    floor_info = raw_data['floor']

    floor_index = floor_info.find('）')
    house_floor = ''  # 5楼层
    house_tall = ''  # 6楼高
    house_decoration = ''  # 7装修方式
    if floor_info != 'None' and floor_info.find("装") == -1:
        if floor_index == -1:
            house_tall = floor_info[1:-1]
        else:
            floor_index_1 = floor_info.find('共')
            house_floor = floor_info[:floor_index_1 - 1]
            floor_index_2 = floor_info.rfind('层')
            real_index = floor_index if floor_index < floor_index_2 else floor_index_2
            house_tall = floor_info[floor_index_1 + 1:real_index]
    elif floor_info.find("装") != -1:
        house_decoration = floor_info
        house_floor = ""
        house_tall = "5"
    elif raw_data['towards'].find("共") != -1:
        floor_info = raw_data['towards']
        floor_index = floor_info.find('）')
        if floor_index == -1:
            house_tall = floor_info[1:-1]
        else:
            floor_index_1 = floor_info.find('共')
            house_floor = floor_info[:floor_index_1 - 1]
            floor_index_2 = floor_info.rfind('层')
            real_index = floor_index if floor_index < floor_index_2 else floor_index_2
            house_tall = floor_info[floor_index_1 + 1:real_index]

    if house_decoration == '' and raw_data['decoration'].find('共') == -1:
        house_decoration = raw_data['decoration']

    tall_float = float(house_tall)

    house_method = raw_data['method']  # 8出租方式
    house_area_0 = raw_data['area_0']  # 9区
    house_area_1 = raw_data['area_1']  # 10县
    house_cell = raw_data['area_2']  # 11小区

    house_towards = "南北向"
    if raw_data['towards'].find('共') == -1:
        house_towards = raw_data['towards']  # 12朝向

    house_latitude = raw_data['latitude']  # 13纬度
    house_longitude = raw_data['longitude']  # 14经度

    longitude_float = float(house_longitude) - 0.012774687519  # 经度修正
    latitude_float = float(house_latitude) - 0.00394531687912  # 纬度修正

    worksheet.write(data_index, 0, house_title)
    worksheet.write(data_index, 1, price_float)
    worksheet.write(data_index, 2, house_unit)
    worksheet.write(data_index, 3, size_float)

    worksheet.write(data_index, 4, house_floor)
    worksheet.write(data_index, 5, tall_float)
    worksheet.write(data_index, 6, house_towards)
    worksheet.write(data_index, 7, house_method)

    worksheet.write(data_index, 8, house_decoration)
    worksheet.write(data_index, 9, house_area_0)
    worksheet.write(data_index, 10, house_area_1)
    worksheet.write(data_index, 11, house_cell)

    worksheet.write(data_index, 12, latitude_float)
    worksheet.write(data_index, 13, longitude_float)

    print(str(data_index))
    data_index += 1

workbook.close()
